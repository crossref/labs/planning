# Labs Planning

This is a meta-project  where Crossre labs can keep track of ideas and projects that we are working on.

It includes a [tracking board](https://gitlab.com/crossref/labs/planning/-/boards/2842483) 

Proposed labels

## Status Labels

- Ideas: Any silly thing that pops into our heads
- Up next: Things we'll do as current projects finished
- In progress: What we are doing
- Live Experiments: Projects that are live for people to explore and play with.
- Retired: Things we did but that we no longer run
- Graduated: Things that got turned into production services

## Sizing labels

- XS (1-5 days)
- S (5-10 days)
- M (10-30 days)
- L (30-90 days)
- XL (90+ days)


## Project type lables

- data-gathering: trying to understand the shape of a problem
- new-technology: testing a new technology that might replace something we currently use
- new-approach: trying new approach to something we currently do:
- new-initiative: trying a new service or an existing or new constituency
- training: learning something
- education: explaining something, exploring something
- outreach: engaging with and understanding a new constituency 



